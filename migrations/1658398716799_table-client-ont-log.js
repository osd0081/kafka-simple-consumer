/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable('client_ont_log', {
    id: {
      type: 'serial',
      primaryKey: true,
    },
    key: {
      type: 'text',
      notNull: false,
    },
    timestamp: {
      type: 'timestamp',
      notNull: true,
    },
  });

  pgm.addConstraint('client_ont_log', 'fk_client_ont_log.log_key.key', 'FOREIGN KEY(key) REFERENCES client_ont(key) ON DELETE CASCADE');
};

exports.down = (pgm) => {
  pgm.dropConstraint('client_ont_log', 'fk_client_ont_log.log_key.key');
  pgm.dropTable('client_ont_log');
};
