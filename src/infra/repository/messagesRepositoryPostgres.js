/* eslint-disable camelcase */

class MessagesRepositoryPostgres {
  constructor(pool) {
    this._pool = pool;
  }

  async UpsertClientOnt({
    key, value, offset, timestamp,
  }) {
    const query = {
      text: 'INSERT INTO client_ont(key,value,"offset",timestamp) VALUES($1,$2,$3,TO_TIMESTAMP($4::BIGINT / 1000)) ON CONFLICT (key) DO UPDATE SET "offset" = excluded.offset, timestamp = excluded.timestamp, value=excluded.value returning key,created_at',
      values: [key, value, offset, timestamp],
    };

    const queryRes = await this._pool.query(query);
    // return queryRes.rowCount;
    return queryRes.rows[0];
  }

  async InsertClientOntLog({
    key, timestamp,
  }) {
    const query = {
      text: 'INSERT INTO client_ont_log(key,timestamp) VALUES($1,TO_TIMESTAMP($2::BIGINT / 1000)) returning timestamp',
      values: [key, timestamp],
    };

    const queryRes = await this._pool.query(query);
    return queryRes.rows[0];
  }

  async CountClientOntLogUpdate(key) {
    const query = {
      text: 'SELECT COUNT(id) as sum FROM client_ont_log WHERE key = $1',
      values: [key],
    };

    const queryRes = await this._pool.query(query);
    return queryRes.rows[0];
  }

  async InsertClient({
    client_mac_address,
    interfaceInfo,
    status,
    type,
    ip_client,
    client_name,
    time_lease,
    rssi,
    snr,
    rx_rate,
    tx_rate,
    key,
  }) {
    const query = {
      text: `INSERT INTO clients(
          client_mac_address,
          interface,
          status,
          type,
          ip_client,
          client_name,
          time_lease,
          rssi,
          snr,
          rx_rate,
          tx_rate,
          key
        ) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)`,
      values: [
        client_mac_address,
        interfaceInfo,
        status,
        type,
        ip_client,
        client_name,
        time_lease,
        rssi,
        snr,
        rx_rate,
        tx_rate,
        key,
      ],
    };
    const queryRes = await this._pool.query(query);
    return queryRes.rowCount;
  }
}

module.exports = MessagesRepositoryPostgres;
