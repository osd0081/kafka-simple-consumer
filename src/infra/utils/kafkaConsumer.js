const kafka = require('node-rdkafka');

function createKafkaConsumer(config) {
  return kafka.KafkaConsumer({
    'group.id': config.group,
    'metadata.broker.list': config.broker,
  }, {});
}

module.exports = createKafkaConsumer;
