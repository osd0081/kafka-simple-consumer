/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable('clients', {
    id: {
      type: 'serial',
      primaryKey: true,
    },
    client_mac_address: {
      type: 'varchar(50)',
      allowNull: true,
    },
    interface: {
      type: 'varchar(50)',
      allowNull: true,
    },
    status: {
      type: 'varchar(50)',
      allowNull: true,
    },
    type: {
      type: 'varchar(50)',
      allowNull: true,
    },
    ip_client: {
      type: 'varchar(50)',
      allowNull: true,
    },
    client_name: {
      type: 'varchar(50)',
      allowNull: true,
    },
    time_lease: {
      type: 'bigint',
      allowNull: true,
    },
    rssi: {
      type: 'integer',
      allowNull: true,
    },
    snr: {
      type: 'numeric',
      allowNull: true,
    },
    rx_rate: {
      type: 'integer',
      allowNull: true,
    },
    tx_rate: {
      type: 'integer',
      allowNull: true,
    },
    key: {
      type: 'text',
      allowNull: false,
    },
  });
  pgm.addConstraint('clients', 'fk_client.client_key.key', 'FOREIGN KEY(key) REFERENCES client_ont(key) ON DELETE CASCADE');
};

exports.down = (pgm) => {
  pgm.dropConstraint('clients', 'fk_client.client_key.key');
  pgm.dropTable('clients');
};
