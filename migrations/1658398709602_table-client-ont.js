/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable('client_ont', {
    id: {
      type: 'serial',
      primaryKey: true,
    },
    offset: {
      type: 'bigint',
      notNull: true,
    },
    key: {
      type: 'varchar(50)',
      unique: true,
      notNull: false,
    },
    value: {
      type: 'text',
      notNull: true,
    },
    timestamp: {
      type: 'timestamp',
      notNull: true,
    },
    created_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
    },
  });
};

exports.down = (pgm) => {
  pgm.dropTable('client_ont');
};
