const testConfig = {
  broker: process.env.BROKER_HOST_DEV,
  group: process.env.CONSUMER_GROUP_DEV,
  topics: [process.env.KAFKA_TOPIC_DEV],
};

const kafkaConsumerConfig = process.env.NODE_ENV === 'test' ? testConfig : {
  broker: process.env.BROKER_HOST,
  group: process.env.CONSUMER_GROUP,
  topics: [process.env.KAFKA_TOPIC],
};

module.exports = kafkaConsumerConfig;
