/* eslint-disable no-console */
require('dotenv').config();
const fs = require('fs');
const pool = require('./infra/config/database/postgres/pool');
const kafkaConsumerConfig = require('./infra/config/messaging/kafka/kafkaConsumerConfig');
const createConsumer = require('./infra/messaging/consumer');
const MessagesRepositoryPostgres = require('./infra/repository/messagesRepositoryPostgres');
const createKafkaConsumer = require('./infra/utils/kafkaConsumer');

require('dotenv').config();

const init = async () => {
  const messagesRepositoryPostgres = new MessagesRepositoryPostgres(pool);
  const kafkaConsumer = createKafkaConsumer(kafkaConsumerConfig);
  const databaseLogging = (err) => {
    fs.writeFile('./log/error-db.txt', err, { flag: 'a+' }, (errWriteErr) => {
      if (errWriteErr) {
        console.log('Error when writing to error-db.txt');
        console.log(errWriteErr, err);
      }
    });
  };

  const consumer = await createConsumer(kafkaConsumer, async ({
    value,
    offset,
    key,
    timestamp,
  }) => {
    console.log(`Data ${key} accepted`);

    // upsert data from value

    const result = await messagesRepositoryPostgres.UpsertClientOnt({
      value,
      offset,
      key,
      timestamp,
    });
    if (!result) {
      databaseLogging(`Upsert data ${key} fail at offset ${offset}`);
      consumer.unsubscribe();
      consumer.disconnect();
    }

    // Log clint-ont update

    const resultLog = await messagesRepositoryPostgres.InsertClientOntLog({
      key,
      timestamp,
    });
    if (!resultLog) {
      databaseLogging(`Logging ${key} fail at offset ${offset}`);
      consumer.unsubscribe();
      consumer.disconnect();
    }

    // Get client-ont connected device data

    const clientDatas = JSON.parse(value.toString());
    Object.keys(clientDatas).forEach(async (clientKey) => {
      const clientResult = await messagesRepositoryPostgres.InsertClient({
        key, ...clientDatas[clientKey],
      });
      if (!clientResult) {
        databaseLogging(`Fail durring ${key} values insert at offset ${offset} on ${clientDatas[clientKey].client_mac_address} `);
        consumer.unsubscribe();
        consumer.disconnect();
      }
    });
  });

  consumer.subscribe(kafkaConsumerConfig.topics);
  consumer.consume();

  // logging all errors
  consumer.on('event.error', (err) => {
    console.log('Error from consumer');
    consumer.unsubscribe();
    consumer.disconnect();

    fs.writeFile('./log/error.txt', err, { flag: 'a+' }, (errWriteErr) => {
      if (errWriteErr) {
        console.log('Error when writing to error.txt');
        console.log(errWriteErr, err);
      }
    });
  });
};

init();
